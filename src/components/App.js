/*eslint import/no-unresolved: off*/

import React, { Component } from 'react';

import Layout from 'Components/layouts/Layout';
import Hero from 'Components/sections/home/Hero';
import FeaturedProducts from 'Components/sections/home/FeaturedProducts';
import HowItWorks from 'Components/sections/home/HowItWorks';
import Gallery from 'Components/sections/home/Gallery';
import Menu from 'Components/sections/home/Menu';
// import Testimonials from './sections/home/Testimonials';
import Packaging from 'Components/sections/home/Packaging';
import Locations from 'Components/sections/home/Locations';

export default class App extends Component {
  render() {
    return (
        <Layout isFixed="true" >
          <main>
            <Hero />
            <FeaturedProducts />
            <Gallery />
            <HowItWorks />
            <Menu />
            <Packaging />
            <Locations />
          </main>
        </Layout>
    );
  }
}