import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import AsyncApp from './AsyncApp';

import Header from '../sections/Header';
import Footer from '../sections/Footer';
import '../../styles/styles.scss';

export default class Layout extends Component {
  static get propTypes() {
    return {
      children: PropTypes.node,
      isFixed: PropTypes.string
    };
  }
  render() {
      return(
         <div>
            <Header isFixed={this.props.isFixed} />
            { this.props.children }
            <Footer />
         </div>
      );
  }
}