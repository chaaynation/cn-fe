import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Contact extends Component {
  render() {
    return (
      <section className="bg-f7f7f7">
        <div className="container py-5 px-md-5">
          <ol className="text-capitalize container pt-2">
            <li className="d-inline-block align-middle">
              <Link className="text-black" to="/">
                <span className="small">Home</span>
              </Link>
              <span className="text-danger small"> &gt; </span>
            </li>
            <li className="d-inline-block align-middle">
              <Link className="text-black" to="/about">
                <span className="small">&nbsp;Contact</span>
              </Link>
            </li>
          </ol>
          <h3 className="text-center pb-1 text-uppercase">
            <div className="d-inline-block py-1 px-4 mb-3">FAQ & Contact</div>
          </h3>
          <div className="pb-5 text-center d-none">Mere paas chai hai, aur tumhare paas kya hai?</div>
          <div className="row">
            <div className="col-md-9">
              <ul className="line-height-2 p-0">
                <li className="bg-white py-3 px-md-5 px-3 mb-3 d-inline-block border-radius-2">
                  <h5 className="font-weight-600 pb-2">Some question goes here and here?</h5>
                  <p>Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
                  <p>Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
                </li>
                <li className="bg-white py-3 px-md-5 px-3 mb-3 d-inline-block border-radius-2">
                  <h5 className="font-weight-600 pb-2">Why ChaayNation ChaayNation ChaayNation ChaayNation ChaayNation ChaayNation ChaayNation ChaayNation ChaayNation ChaayNation?</h5>
                  <p>Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
                  <p>Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
                </li>
                <li className="bg-white py-3 px-md-5 px-3 mb-3 d-inline-block border-radius-2">
                  <h5 className="font-weight-600 pb-2">Some things goes here?</h5>
                  <p>Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
                  <p>Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
                </li>
                <li className="bg-white py-3 px-md-5 px-3 mb-3 d-inline-block border-radius-2">
                  <h5 className="font-weight-600 pb-2">Some things goes here?</h5>
                  <p>Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
                  <p>Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
                </li>
                <li className="bg-white py-3 px-md-5 px-3 mb-3 d-inline-block border-radius-2">
                  <h5 className="font-weight-600 pb-2">Some things goes here?</h5>
                  <p>Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
                  <p>Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
                </li>
                <li className="bg-white py-3 px-md-5 px-3 mb-3 d-inline-block border-radius-2">
                  <h5 className="font-weight-600 pb-2">Some things goes here?</h5>
                  <p>Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
                  <p>Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
                </li>
              </ul>
            </div>
            <div className="col-md-3">
              <ul className="line-height-2 px-3 bg-white border-radius-2">
                <li className="py-2 d-block">
                  <Link to="https://www.facebook.com/chaaynation/">
                    <i className="text-fb fab fa-facebook-f" aria-hidden="true" />
                    <span className="pl-2 text-black">Facebook</span>
                  </Link>
                </li>
                <li className="py-2 d-block">
                  <Link to="https://www.instagram.com/chaaynation/">
                    <i className="text-instagram fab fa-instagram" aria-hidden="true" />
                    <span className="pl-2 text-black">Instagram</span>
                  </Link>
                </li>
                <li className="py-2 d-block">
                  <Link to="https://twitter.com/chaaynation">
                    <i className="text-twitter fab fa-twitter" aria-hidden="true" />
                    <span className="pl-2 text-black">Twitter</span>
                  </Link>
                </li>
                <li className="py-2 d-block">
                  <Link to="https://www.pinterest.com/chaaynation/">
                    <i className="text-pinterest fab fa-pinterest" aria-hidden="true" />
                    <span className="pl-2 text-black">Pinterest</span>
                  </Link>
                </li>
                <li className="py-2 d-block">
                  <a href="mailto:hi@chaaynation.com">
                    <i className="text-primary far fa-envelope" aria-hidden="true" />
                    <span className="pl-2 text-black">Email</span>
                  </a>
                </li>
                <li className="py-2 d-block">
                  <a href="tel:+1800123456">
                    <i className="text-danger fas fa-phone" aria-hidden="true" />
                    <span className="pl-2 text-black">1-800-1HOT-TEA</span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
    );
  }
}