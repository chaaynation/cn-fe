/*eslint import/no-unresolved: off*/

import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import MasalaChaiJPG from 'Assets/masala-chai.jpg';
import CardamomChaiJPG from 'Assets/cardamom-chai.jpg';
import GingerChaiJPEG from 'Assets/ginger-chai.jpeg';
import KashmiriChaiPNG from 'Assets/kashmiri-chai.png';

export default class FeaturedProducts extends Component {
  render() {
    return (
      <section className="p-5 bg-f7f7f7 featured-products">
        <div className="container">
          <h3 className="text-center pb-1 text-uppercase">Featured Chai's</h3>
          <div className="pb-5 text-center">Here’s a list of the most popular types of chai you can try from our menu.</div>
          <div className="row">
            <div className="col-md-6 col-lg-3 mb-3">
              <div className="px-4 py-5 bg-white">
                <Link to="/product/masala-chai" className="text-black text-decoration-none">
                  <div className="pb-4 text-center">
                    <div className="text-uppercase font-weight-600">Masala Chai</div>
                    <div className="small text-capitalize text-9f9d93">made with Spices</div>
                  </div>
                  <img src={MasalaChaiJPG} className="product-image" />
                  <div className="pt-4 text-center small">$3.00</div>
                </Link>
              </div>
            </div>
            <div className="col-md-6 col-lg-3 mb-3">
              <div className="px-4 py-5 bg-white">
                <Link to="/product/cardamom-chai" className="text-black text-decoration-none">
                  <div className="pb-4 text-center">
                    <div className="text-uppercase font-weight-600">Elaichi Chai</div>
                    <div className="small text-capitalize text-9f9d93">made with Cardamom</div>
                  </div>
                  <img src={CardamomChaiJPG} className="product-image" />
                  <div className="pt-4 text-center small">$2.75</div>
                </Link>
              </div>
            </div>
            <div className="col-md-6 col-lg-3 mb-3">
              <div className="px-4 py-5 bg-white">
                <Link to="/product/ginger-chai" className="text-black text-decoration-none">
                  <div className="pb-4 text-center">
                    <div className="text-uppercase font-weight-600">Adrak Chai</div>
                    <div className="small text-capitalize text-9f9d93">made with Ginger</div>
                  </div>
                  <img src={GingerChaiJPEG} className="product-image" />
                  <div className="pt-4 text-center small">$1.75</div>
                </Link>
              </div>
            </div>
            <div className="col-md-6 col-lg-3 mb-3">
              <div className="px-4 py-5 bg-white">
                <Link to="/product/kashmiri-kanwa" className="text-black text-decoration-none">
                  <div className="pb-4 text-center">
                    <div className="text-uppercase font-weight-600">Kashmiri Kahwa</div>
                    <div className="small text-capitalize text-9f9d93">made with Saffron & Spices</div>
                  </div>
                  <img src={KashmiriChaiPNG} className="product-image" />
                  <div className="pt-4 text-center small">$3.45</div>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}