import React, { Component } from 'react';

export default class HowItWorks extends Component {
  constructor(props) {
    super(props);
    this.handleScroll = this.handleScroll.bind(this);
  }
  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll, false);
  }
  handleScroll() {
    let element = document.getElementsByClassName('progress')[0];
    let rect = element.getBoundingClientRect();
    let html = document.documentElement;
    if (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || html.clientHeight) &&
      rect.right <= (window.innerWidth || html.clientWidth)
    ) {
      let $points    = $('.progress-points').first();
      let val     = +$points.data('current') - 1;
      
      this.activate(val);
    }
  }
  activate(index) {
    let $point_arr, $progress, active, max;

    $point_arr = $('.progress-point');
    $progress  = $('.progress').first();

    max     = $point_arr.length - 1;
    active = 0;
    if (index !== active) {
      active       = index;
      let $_active = $point_arr.eq(active);

      $point_arr
        .removeClass('completed active')
        .slice(0, active).addClass('completed');

      $_active.addClass('completed');

      return $progress.css('width', (index / max * 100) + "%");
    }
  }
  render() {
    return (
      <section className="p-5 bg-white how-it-works">
        <h3 className="text-center pb-1 text-uppercase">How it works</h3>
        <div className="pb-5 text-center">Chai Chai Chai, Duniya Bhaad Mein Jaye</div>
        <div className="container d-block d-lg-none">
          <div className="row">
            <div className="col-md-12 col-lg-3 mb-3">
              <div className="px-4 py-5 bg-f7f7f7">
                <div className="text-center">
                  <i className="fas fa-coffee font-size-108px text-ca7b49" />
                </div>
                <div className="pt-4 text-center font-weight-500">1. Select your chai</div>
              </div>
            </div>
            <div className="col-md-12 col-lg-3 mb-3">
              <div className="px-4 py-5 bg-f7f7f7">
                <div className="text-center">
                  <i className="fas fa-shopping-cart font-size-108px text-3eb51a" />
                </div>
                <div className="pt-4 text-center font-weight-500">2. Checkout</div>
              </div>
            </div>
            <div className="col-md-12 col-lg-3 mb-3">
              <div className="px-4 py-5 bg-f7f7f7">
                <div className="text-center">
                  <i className="fas fa-shopping-bag font-size-108px text-e67686" />
                </div>
                <div className="pt-4 text-center font-weight-500">3. Get it in 30 mins</div>
              </div>
            </div>
            <div className="col-md-12 col-lg-3 mb-3">
              <div className="px-4 py-5 bg-f7f7f7">
                <div className="text-center">
                  <i className="fas fa-child font-size-108px text-748ffc" />
                </div>
                <div className="pt-4 text-center font-weight-500">4. Enjoy your chai!!!</div>
              </div>
            </div>
          </div>
        </div>
        <div className="d-none d-lg-block container">
          <div className="progress-meter">
            <div className="track">
              <span className="progress" />
            </div>
            <ol className="progress-points" data-current="4">
              <li className="progress-point">
                <div className="text-center">
                  <i className="fas fa-coffee fa-10x text-ca7b49" />
                </div>
                <div className="pt-3 font-weight-500 label">Select your chai</div>
              </li>
              <li className="progress-point">
                <div className="text-center">
                  <i className="fas fa-shopping-cart fa-10x text-3eb51a" />
                </div>
                <div className="pt-3 font-weight-500 label">Checkout</div>
              </li>
              <li className="progress-point">
                <div className="text-center">
                  <i className="fas fa-shopping-bag fa-10x text-e67686" />
                </div>
                <div className="pt-3 font-weight-500 label">Get it in 30 mins</div>
              </li>
              <li className="progress-point">
                <div className="text-center">
                  <i className="fas fa-child fa-10x text-748ffc" />
                </div>
                <div className="pt-3 font-weight-500 label">Enjoy your chai!!!</div>
              </li>
            </ol>
          </div>
        </div>
      </section>
    );
  }
}