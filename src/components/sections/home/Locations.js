import React, { Component } from 'react';

export default class Packaging extends Component {
  render() {
    return (
      <section className="py-5 bg-f7f7f7 featured-locations location-image">
        <div className="modal">
            <div className="modal-dialog" role="document">
              <div className="modal-content">
                <div className="modal-body text-center">
                  <h3 className="text-center py-3 text-uppercase">Locations and Hours</h3>
                  <p><i className="far fa-calendar mr-2 text-ca7b49" />Monday - Friday</p>
                  <p><i className="far fa-clock mr-2 text-ca7b49" />4pm - 6pm</p>
                  <p><i className="fas fa-map-marker mr-2 text-ca7b49" />721 First Ave Sunnyvale CA</p>
                  <p><i className="fas fa-map-marker mr-2 text-ca7b49" />450 N Mathilda Ave Sunnyvale CA</p>
                  <p><i className="fas fa-map-marker mr-2 text-ca7b49" />102 E Evelyn Ave Sunnyvale CA</p>
                  <p><i className="fas fa-map-marker mr-2 text-ca7b49" />629 Santa Paula Ave Sunnyvale CA</p>
                </div>
              </div>
            </div>
          </div>
      </section>
    );
  }
}