import React, { Component } from 'react';
// import { Link } from 'react-router-dom';

export default class Testimonials extends Component {
  render() {
    return (
      <section className="pt-5 px-5 bg-white featured-testimonials">
        <div className="container">
          <div className="row">
            <div className="col-sm-8 testimonials-queue-image" />
            <div className="col-sm-4 testimonials-cart-image" />
          </div>
        </div>
      </section>
    );
  }
}