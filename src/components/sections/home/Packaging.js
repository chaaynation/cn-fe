import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Packaging extends Component {
  render() {
    return (
      <section className="py-5 px-5 bg-white featured-packaging">
        <div className="container">
          <h3 className="text-center pb-5 text-uppercase">Packaging</h3>
          <div className="row">
            <div className="col-sm-6">
              <img src="../../assets/Butterfly-Cup.png" className="img-fluid" />
            </div>
            <div className="col-sm-6 position-relative">
              <div className="pt-5">
                <div className="pb-3"> We use <Link to="https://mybutterflycup.com/">Butterfly Cup</Link>, as it has multiple benefits.</div>
                <ul className="justify-content-center">
                  <li className="pb-2">Ease of use</li>
                  <li className="pb-2">No Splashes, leaks or drips</li>
                  <li className="pb-2">Reduces carbon footprint</li>
                  <li className="pb-2">No Plastic</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}