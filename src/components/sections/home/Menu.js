import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Menu extends Component {
  render() {
    return (
      <section className="p-5 bg-f7f7f7 featured-menu">
        <div className="container bg-fff9f0 p-5">
          <h3 className="text-center pb-1 text-uppercase">
            <div className="d-inline-block py-1 px-4 bordery-double">Menu</div></h3>
          <div className="pb-5 text-center">Mere paas chai hai, aur tumhare paas kya hai?</div>
          <div className="row">
            <ul className="col-md-6 mb-0 px-5">
              <li className="product row pb-4">
                  <div className="product-title col-sm-10 font-weight-600 pb-1">
                    <Link className="text-black" to="/product/masala-chai">Masala Chai</Link>
                  </div>
                  <div className="product-price col-sm-2">$2.75</div>
                  <div className="col-sm-10 text-9f9d93 product-ingredients small">
                    <div>A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions</div>
                  </div>
              </li>
              <li className="product row pb-4">
                <div className="product-title col-sm-10 font-weight-600 pb-1">
                    <Link className="text-black" to="/product/ginger-chai">Adrak or Ginger Chai</Link>
                  </div>
                <div className="product-price col-sm-2">$3.75</div>
                <div className="col-sm-10 text-9f9d93 product-ingredients small">
                  <div>Grated ginger is added to the tea pot while the tea is brewing. This is also considered the elixir for colds because of ginger’s reputation for clearing sinuses. </div>
                </div>
              </li>
              <li className="product row pb-4">
                  <div className="product-title col-sm-10 font-weight-600 pb-1">
                    <Link className="text-black" to="/product/cardamom-chai">Elaichi or Cardamom Chai</Link>
                  </div>
                  <div className="product-price col-sm-2">$2.50</div>
                  <div className="col-sm-10 text-9f9d93 product-ingredients small">
                    <div>It’s sweet from the cardamom and is a refreshing choice of chai. Cinnamon can also added grated or powdered. </div>
                  </div>
              </li>
              <li className="product row pb-4">
                <div className="product-title col-sm-10 font-weight-600 pb-1">
                    <Link className="text-black" to="/product/bombay-cutting-chai">Bombay Cutting Chai</Link>
                  </div>
                <div className="product-price col-sm-2">$1.75</div>
                <div className="col-sm-10 text-9f9d93 product-ingredients small">
                  <div>It’s a modified masala chai. The flavor of the tea is so strong that it is only served by the half-glass.</div>
                </div>
              </li>
            </ul>
            <ul className="col-md-6 mb-0 px-5">
              <li className="product row pb-4">
                  <div className="product-title col-sm-10 font-weight-600 pb-1">
                    <Link className="text-black" to="/product/kashmiri-kahwa">Kashmiri Kahwa</Link>
                  </div>
                  <div className="product-price col-sm-2">$4.00</div>
                  <div className="col-sm-10 text-9f9d93 product-ingredients small">
                    <div>Kashmiri Kahwa is derived from saffron strands, which are included in the blend. Cardamom, almonds, cinnamon and cloves are also added to the tea.</div>
                  </div>
              </li>
              <li className="product row pb-4">
                <div className="product-title col-sm-10 font-weight-600 pb-1">
                    <Link className="text-black" to="/product/sulaimani-chai">Sulaimani Chai</Link>
                  </div>
                <div className="product-price col-sm-2">$2.75</div>
                <div className="col-sm-10 text-9f9d93 product-ingredients small">
                  <div>The Sulaimani is basically black tea with lemon. Also, known as ‘Ghava’ or ‘Kattan Chaya’, this is a form of black tea which finds its roots in South India.</div>
                </div>
              </li>
              <li className="product row pb-4">
                  <div className="product-title col-sm-10 font-weight-600 pb-1">
                    <Link className="text-black" to="/product/tulsi-chai">Tulsi Chai</Link>
                  </div>
                  <div className="product-price col-sm-2">$1.75</div>
                  <div className="col-sm-10 text-9f9d93 product-ingredients small">
                    <div>Fresh tulsi leaves are added to the brew and it’s thought to strengthen the immune system and in relieving stress.</div>
                  </div>
              </li>
              <li className="product row pb-4">
                <div className="product-title col-sm-10 font-weight-600 pb-1">
                  <Link className="text-black" to="/product/green-chai">Green Tea</Link>
                </div>
                <div className="product-price col-sm-2">$3.75</div>
                <div className="col-sm-10 text-9f9d93 product-ingredients small">
                  <div>Bright flavors of green tea good for health.</div>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div className="container menu-bottom-image bg-fff9f0" />
      </section>
    );
  }
}