import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

export default class Hero extends Component {
  render() {
    return (
      <section className="hero">
        <div className="vid-overlay">
          <div className="hero-txt">
            <div>Chaay at your location.</div>
            <ul className="justify-content-center">
              <li className="d-inline-block mr-4">
                <NavLink to="/products" className="btn btn-primary">
                  <span className="pb-2 text-uppercase">Menu</span>
                </NavLink>
              </li>
              <li className="d-inline-block">
                <NavLink to="/locations" className="btn btn-primary">
                  <span className="pb-2 text-uppercase">Locations</span>
                </NavLink>
              </li>
            </ul>
          </div>
          <div className="scroll">
            <i className="fa fa-chevron-down" />
          </div>
        </div>
        <video src="assets/chai.mp4" autoPlay loop muted poster="assets/chai.gif" width="100%" title="Chaay Nation">
          Sorry, your browser does not support embedded videos,
          but do not worry, you can <a href="assets/chai.mp4">download it</a>
          and watch it with your favorite video player!
        </video>
      </section>
    );
  }
}