/*eslint import/no-unresolved: off*/

import React, { Component } from 'react';

import MasalaChaiJPG from 'Assets/masala-chai.jpg';
import CardamomChaiJPG from 'Assets/cardamom-chai.jpg';
import GingerChaiJPEG from 'Assets/ginger-chai.jpeg';
import KashmiriChaiPNG from 'Assets/kashmiri-chai.png';

export default class Gallery extends Component {
  render() {
    return (
      <section className="featured-gallery">
        <div className="container-fluid">
          <div className="row mb-1">
            <div className="col py-0 pr-1 pl-1">
              <img src={MasalaChaiJPG} className="img-fluid" />
            </div>
            <div className="col py-0 pr-0 pl-0">
              <img src={CardamomChaiJPG} className="img-fluid" />
            </div>
            <div className="col py-0 pr-1 pl-0">
              <img src={GingerChaiJPEG} className="img-fluid" />
            </div>
            <div className="col py-0 pr-1 pl-0">
              <img src={KashmiriChaiPNG} className="img-fluid" />
            </div>
          </div>
          <div className="row">
            <div className="col py-0 pr-0 pl-1">
              <img src={KashmiriChaiPNG} className="img-fluid" />
            </div>
            <div className="col py-0 pr-1 pl-0">
              <img src={GingerChaiJPEG} className="img-fluid" />
            </div>
            <div className="col py-0 pr-0 pl-0">
              <img src={CardamomChaiJPG} className="img-fluid" />
            </div>
            <div className="col py-0 pr-1 pl-0">
              <img src={MasalaChaiJPG} className="img-fluid" />
            </div>
          </div>
        </div>
      </section>
    );
  }
}