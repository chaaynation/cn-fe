import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class AboutCompany extends Component {
  render() {
    return (
      <section className="bg-f7f7f7">
        <div className="container p-4">
          <ol className="text-capitalize container pt-2">
            <li className="d-inline-block align-middle">
              <Link className="text-black" to="/">
                <span className="small">Home</span>
              </Link>
              <span className="text-danger small"> &gt; </span>
            </li>
            <li className="d-inline-block align-middle">
              <Link className="text-black" to="/about">
                <span className="small">&nbsp;About</span>
              </Link>
            </li>
          </ol>
          <h3 className="text-center pb-1 text-uppercase d-none">
            <div className="d-inline-block py-1 px-4 mb-3">About Us</div>
          </h3>
          <div className="pb-5 text-center d-none">Mere paas chai hai, aur tumhare paas kya hai?</div>
          <ul className="line-height-2 p-0">
            <li className="bg-white py-3 px-sm-5  px-2 mb-3 d-inline-block border-radius-2">
              <h5 className="text-capitalize text-center border-4-double-white py-2 bg-product">About ChaayNation</h5>
              <div className="px-md-5 py-3">
                <p className="">Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
                <p className="">Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
              </div>
            </li>
            <li className="bg-white py-3 px-sm-5  px-2 mb-3 d-inline-block border-radius-2">
              <h5 className="text-capitalize text-center border-4-double-white py-2 bg-product">Why ChaayNation?</h5>
              <div className="px-md-5 py-3">
                <p className="">Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
                <p className="">Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
              </div>
            </li>
            <li className="bg-white py-3 px-sm-5  px-2 mb-3 d-inline-block border-radius-2">
              <h5 className="text-capitalize text-center border-4-double-white py-2 bg-product">Some things goes here?</h5>
              <div className="px-md-5 py-3">
                <p className="">Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
                <p className="">Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
              </div>
            </li>
            <li className="bg-white py-3 px-sm-5  px-2 mb-3 d-inline-block border-radius-2">
              <h5 className="text-capitalize text-center border-4-double-white py-2 bg-product">Some things goes here?</h5>
              <div className="px-md-5 py-3">
                <p className="">Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
                <p className="">Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
              </div>
            </li>
            <li className="bg-white py-3 px-sm-5  px-2 mb-3 d-inline-block border-radius-2">
              <h5 className="text-capitalize text-center border-4-double-white py-2 bg-product">Some things goes here?</h5>
              <div className="px-md-5 py-3">
                <p className="">Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
                <p className="">Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
              </div>
            </li>
            <li className="bg-white py-3 px-sm-5  px-2 mb-3 d-inline-block border-radius-2">
              <h5 className="text-capitalize text-center border-4-double-white py-2 bg-product">Some things goes here?</h5>
              <div className="px-md-5 py-3">
                <p className="">Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
                <p className="">Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here Some things goes here.</p>
              </div>
            </li>
          </ul>
        </div>
      </section>
    );
  }
}