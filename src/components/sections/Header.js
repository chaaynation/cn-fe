import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

export default class Header extends Component {
  static get propTypes() {
    return {
      isFixed: PropTypes.string
    };
  }
  render() {
    let isFixed = this.props.isFixed === "true" ? 'position-absolute' : 'position-relative bg-header';
    return (
      <header className={isFixed}>
        <nav className="navbar navbar-expand-lg">
          <NavLink to="/" className="nav-link navbar-brand mr-0 d-lg-none text-white">
            Chaay Nation
          </NavLink>
          <button className="navbar-toggler bg-white" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i className="fa fa-bars fa-lg" />
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav nav w-100 justify-content-center">
              <li className="nav-item">
                <NavLink to="/" className="nav-link">
                  <span className="pb-2 small-caps">Home</span>
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/products" className="nav-link">
                  <span className="pb-2 small-caps">Menu</span>
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/locations" className="nav-link">
                  <span className="pb-2 small-caps">Locations</span>
                </NavLink>
              </li>
              <li className="nav-item brand d-none d-lg-block">
                <NavLink to="/" className="nav-link navbar-brand mr-0">
                  Chaay Nation
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/how-it-works" className="nav-link">
                  <span className="pb-2 small-caps">How it Works</span>
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/about" className="nav-link">
                  <span className="pb-2 small-caps">About</span>
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink to="/contact" className="nav-link">
                  <span className="pb-2 small-caps">Contact</span>
                </NavLink>
              </li>
            </ul>
          </div>
        </nav>
      </header>
    );
  }
}