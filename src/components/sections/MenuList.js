/*eslint import/no-unresolved: off*/

import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import MasalaChaiJPG from 'Assets/masala-chai.jpg';
import CardamomChaiJPG from 'Assets/cardamom-chai.jpg';
import GingerChaiJPEG from 'Assets/ginger-chai.jpeg';
import KashmiriChaiPNG from 'Assets/kashmiri-chai.png';

export default class MenuList extends Component {
  render() {
    return (
      <section className="bg-f7f7f7">
        <div className="container py-4">
          <ol className="text-capitalize container pt-2">
            <li className="d-inline-block align-middle">
              <Link className="text-black" to="/">
                <span className="small">Home</span>
              </Link>
              <span className="text-danger small"> &gt; </span>
            </li>
            <li className="d-inline-block align-middle">
              <Link className="text-black" to="/products">
                <span className="small">&nbsp;Menu</span>
              </Link>
            </li>
          </ol>
          <h3 className="text-center pb-1 text-uppercase">
            <div className="d-inline-block py-1 px-4">Menu</div></h3>
          <div className="pb-5 text-center">Mere paas chai hai, aur tumhare paas kya hai?</div>
          <div className="row">
            <div className="col-12 mb-3">
              <Link  to="/product/masala-chai" className="product-wrapper bg-white p-3 row">
                <div className="img-fluid col-3">
                  <img src={MasalaChaiJPG} className="img-fluid" />
                </div>
                <div className="product-title font-weight-600 col-9">
                  <h5 className="text-black pb-2">
                    Masala Chai
                    <i className="far fa-heart text-danger float-right small" />
                  </h5>
                  <div className="text-black pb-1 small">A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions. A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions.A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions</div>
                  <div className="text-black">$2.75</div>
                </div>
              </Link>
            </div>
            <div className="col-12 mb-3">
              <Link to="/product/ginger-chai" className="product-wrapper bg-white p-3 row">
                <div className="img-fluid col-3">
                  <img src={CardamomChaiJPG} className="img-fluid" />
                </div>
                <div className="product-title font-weight-600 col-9">
                  <h5 className="text-black pb-2">
                    Adrak or Ginger Chai
                    <i className="far fa-heart text-danger float-right small" />
                  </h5>
                  <div className="text-black pb-1 small">A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions. A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions.A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions</div>
                  <div className="text-black">$3.75</div>
                </div>
              </Link>
            </div>
            <div className="col-12 mb-3">
              <Link className="product-wrapper bg-white p-3 row" to="/product/cardamom-chai">
                <div className="img-fluid col-3">
                  <img src={GingerChaiJPEG} className="img-fluid" />
                </div>
                <div className="product-title font-weight-600 col-9">
                  <h5 className="text-black pb-2">
                    Elaichi or Cardamom Chai
                    <i className="far fa-heart text-danger float-right small" />
                  </h5>
                  <div className="text-black pb-1 small">A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions. A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions.A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions</div>
                  <div className="text-black">$2.50</div>
                </div>
              </Link>
            </div>
            <div className="col-12 mb-3">
              <Link className="product-wrapper bg-white p-3 row" to="/product/bombay-cutting-chai">
                <div className="img-fluid col-3">
                  <img src={KashmiriChaiPNG} className="img-fluid" />
                </div>
                <div className="product-title font-weight-600 col-9">
                  <h5 className="text-black pb-2">
                    Bombay Cutting Chai
                    <i className="far fa-heart text-danger float-right small" />
                  </h5>
                  <div className="text-black pb-1 small">A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions. A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions.A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions</div>
                  <div className="text-black">$1.75</div>
                </div>
              </Link>
            </div>
            <div className="col-12 mb-3">
              <Link className="product-wrapper bg-white p-3 row" to="/product/kashmiri-kahwa">
                <div className="img-fluid col-3">
                  <img src={CardamomChaiJPG} className="img-fluid" />
                </div>
                <div className="product-title font-weight-600 col-9">
                  <h5 className="text-black pb-2">
                    Kashmiri Kahwa
                    <i className="far fa-heart text-danger float-right small" />
                  </h5>
                  <div className="text-black pb-1 small">A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions. A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions.A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions</div>
                  <div className="text-black">$4.00</div>
                </div>
              </Link>
            </div>
            <div className="col-12 mb-3">
              <Link className="product-wrapper bg-white p-3 row" to="/product/sulaimani-chai">
                <div className="img-fluid col-3">
                  <img src={MasalaChaiJPG} className="img-fluid" />
                </div>
                <div className="product-title font-weight-600 col-9">
                  <h5 className="text-black pb-2">
                    Sulaimani Chai
                    <i className="far fa-heart text-danger float-right small" />
                  </h5>
                  <div className="text-black pb-1 small">A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions. A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions.A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions</div>
                  <div className="text-black">$2.75</div>
                </div>
              </Link>
            </div>
            <div className="col-12 mb-3">
              <Link className="product-wrapper bg-white p-3 row" to="/product/tulsi-chai">
                <div className="img-fluid col-3">
                  <img src={GingerChaiJPEG} className="img-fluid" />
                </div>
                <div className="product-title font-weight-600 col-9">
                  <h5 className="text-black pb-2">
                    Tulsi Chai
                    <i className="far fa-heart text-danger float-right small" />
                  </h5>
                  <div className="text-black pb-1 small">A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions. A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions.A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions</div>
                  <div className="text-black">$1.75</div>
                </div>
              </Link>
            </div>
            <div className="col-12 mb-3">
              <Link className="product-wrapper bg-white p-3 row" to="/product/green-chai">
                <div className="img-fluid col-3">
                  <img src={KashmiriChaiPNG} className="img-fluid" />
                </div>
                <div className="product-title font-weight-600 col-9">
                  <h5 className="text-black pb-2">
                    Green Tea
                    <i className="far fa-heart text-danger float-right small" />
                  </h5>
                  <div className="text-black pb-1 small">A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions. A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions.A tea spiced with Indian flavors like cardamom, clove, black pepper, cinnamon, ginger and nutmeg in varying proportions</div>
                  <div className="text-black">$3.75</div>
                </div>
              </Link>
            </div>
          </div>
        </div>
        <div className="container py-4">
          <h3 className="text-center pb-1 text-uppercase">
            <div className="d-inline-block py-1 px-4">Menu</div></h3>
          <div className="pb-5 text-center">Mere paas chai hai, aur tumhare paas kya hai?</div>
          <div className="row text-center">
            <div className="col-md-4 mb-3 pr-0">
              <Link  to="/product/masala-chai" className="product-wrapper bg-white p-3 d-block">
                <img src={MasalaChaiJPG} className="img-fluid" />
                <div className="product-title font-weight-600 pt-3 pb-1">
                  <div className="text-black pb-1">Masala Chai</div>
                  <div className="text-black small">A tea spiced with cardamom, clove, black pepper, cinnamon, ginger and nutmeg</div>
                </div>
                <div className="text-9f9d93 small">$2.75</div>
              </Link>
            </div>
            <div className="col-md-4 mb-3 pr-0">
              <Link to="/product/ginger-chai" className="product-wrapper bg-white p-3 d-block">
                <img src={KashmiriChaiPNG} className="img-fluid" />
                <div className="product-title font-weight-600 pt-3 pb-1">
                  <div className="text-black pb-1">Adrak or Ginger Chai</div>
                  <div className="text-black small">A tea spiced with cardamom, clove, black pepper, cinnamon, ginger and nutmeg</div>
                </div>
                <div className="text-9f9d93 small">$3.75</div>
              </Link>
            </div>
            <div className="col-md-4 mb-3 pr-0">
              <Link className="product-wrapper bg-white p-3 d-block" to="/product/cardamom-chai">
                <img src={CardamomChaiJPG} className="img-fluid" />
                <div className="product-title font-weight-600 pt-3 pb-1">
                  <div className="text-black pb-1">Elaichi or Cardamom Chai</div>
                  <div className="text-black small">A tea spiced with cardamom, clove, black pepper, cinnamon, ginger and nutmeg</div>
                </div>
                <div className="text-9f9d93 small">$2.50</div>
              </Link>
            </div>
            <div className="col-md-4 mb-3 pr-0">
              <Link className="product-wrapper bg-white p-3 d-block" to="/product/bombay-cutting-chai">
                <img src={GingerChaiJPEG} className="img-fluid" />
                <div className="product-title font-weight-600 pt-3 pb-1">
                  <div className="text-black pb-1">Bombay Cutting Chai</div>
                  <div className="text-black small">A tea spiced with cardamom, clove, black pepper, cinnamon, ginger and nutmeg</div>
                </div>
                <div className="text-9f9d93 small">$1.75</div>
              </Link>
            </div>
            <div className="col-md-4 mb-3 pr-0">
              <Link className="product-wrapper bg-white p-3 d-block" to="/product/kashmiri-kahwa">
                <img src={CardamomChaiJPG} className="img-fluid" />
                <div className="product-title font-weight-600 pt-3 pb-1">
                  <div className="text-black pb-1">Kashmiri Kahwa</div>
                  <div className="text-black small">A tea spiced with cardamom, clove, black pepper, cinnamon, ginger and nutmeg</div>
                </div>
                <div className="text-9f9d93 small">$4.00</div>
              </Link>
            </div>
            <div className="col-md-4 mb-3 pr-0">
              <Link className="product-wrapper bg-white p-3 d-block" to="/product/sulaimani-chai">
                <img src={MasalaChaiJPG} className="img-fluid" />
                <div className="product-title font-weight-600 pt-3 pb-1">
                  <div className="text-black pb-1">Sulaimani Chai</div>
                  <div className="text-black small">A tea spiced with cardamom, clove, black pepper, cinnamon, ginger and nutmeg</div>
                </div>
                <div className="text-9f9d93 small">$2.75</div>
              </Link>
            </div>
            <div className="col-md-4 mb-3 pr-0">
              <Link className="product-wrapper bg-white p-3 d-block" to="/product/tulsi-chai">
                <img src={GingerChaiJPEG} className="img-fluid" />
                <div className="product-title font-weight-600 pt-3 pb-1">
                  <div className="text-black pb-1">Tulsi Chai</div>
                  <div className="text-black small">A tea spiced with cardamom, clove, black pepper, cinnamon, ginger and nutmeg</div>
                </div>
                <div className="text-9f9d93 small">$1.75</div>
              </Link>
            </div>
            <div className="col-md-4 mb-3 pr-0">
              <Link className="product-wrapper bg-white p-3 d-block" to="/product/green-chai">
                <img src={KashmiriChaiPNG} className="img-fluid" />
                <div className="product-title font-weight-600 pt-3 pb-1">
                  <div className="text-black pb-1">Green Tea</div>
                  <div className="text-black small">A tea spiced with cardamom, clove, black pepper, cinnamon, ginger and nutmeg</div>
                </div>
                <div className="text-9f9d93 small">$3.75</div>
              </Link>
            </div>
          </div>
        </div>
      </section>
    );
  }
}