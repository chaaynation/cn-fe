import React from 'react';
import { Link } from 'react-router-dom';

const Footer = () => (
  <footer className="p-5 bg-white">
    <div className="container">
      <div className="footer-social text-center">
        <div className="d-inline-block mr-4">
          <Link to="https://www.facebook.com/chaaynation/">
            <i className="text-fb fab fa-facebook-f" aria-hidden="true" />
          </Link>
        </div>
        <div className="d-inline-block mr-4">
          <Link to="https://www.instagram.com/chaaynation/">
            <i className="text-instagram fab fa-instagram" aria-hidden="true" />
          </Link>
        </div>
        <div className="d-inline-block mr-4">
          <Link to="https://twitter.com/chaaynation">
            <i className="text-twitter fab fa-twitter" aria-hidden="true" />
          </Link>
        </div>
        <div className="d-inline-block mr-4">
          <Link to="https://www.pinterest.com/chaaynation/">
            <i className="text-pinterest fab fa-pinterest" aria-hidden="true" />
          </Link>
        </div>
      </div>
      <div className="text-center mt-5 pt-3 border-top-1px">
        Copyright © 2018 ChaayNation. All Rights Reserved.
      </div>
    </div>
  </footer>
);

export default Footer;