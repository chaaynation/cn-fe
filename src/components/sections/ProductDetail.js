/*eslint import/no-unresolved: off*/

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import FeaturedProducts from 'Components/sections/home/FeaturedProducts';

import MasalaChaiJPG from 'Assets/masala-chai.jpg';
import GingerChaiJPEG from 'Assets/ginger-chai.jpeg';
import KashmiriChaiPNG from 'Assets/kashmiri-chai.png';
import CinnamonChaiJPG from 'Assets/cinnamon-chai.jpg';
import CinnamonJPG from 'Assets/cinnamon.jpg';
import GingerJPG from 'Assets/ginger.jpg';
import CardamomJPG from 'Assets/cardamom.jpg';
import NutmegJPG from 'Assets/nutmeg.jpg';
import BlackPepperJPG from 'Assets/black-pepper.jpg';
import CloveJPG from 'Assets/clove.jpg';
import MilkJPG from 'Assets/milk.jpg';
import TeaJPG from 'Assets/tea.jpg';
import SugarJPG from 'Assets/sugar.jpg';

export default class Product extends Component {
  static get propTypes() {
    return {
      loc: PropTypes.string
    };
  }
  handleClick() {
    // console.log(this.currentTarget);
  }
  render() {
    let loc = this.props.loc;
    loc = loc.split('/');
    let item = loc[loc.length-1];
    item = item.replace('-', ' ');
    return (
      <div className="container">
        <div className="row">
          <ol className="col-md-6 text-capitalize">
            <li className="d-inline-block align-middle">
              <Link className="text-black" to="/">
                <span className="small">Home</span>
              </Link>
              <span className="text-danger small"> &gt; </span>
            </li>
            <li className="d-inline-block align-middle">
              <Link className="text-black" to="/products">
                <span className="small">&nbsp;Menu</span>
              </Link>
              <span className="text-danger small"> &gt; </span>
            </li>
            <li className="d-inline-block align-middle">
              <Link className="text-black" to={this.props.loc}>
                <span className="small">&nbsp;{item}</span>
              </Link>
              <span className="text-danger small"> &gt; </span>
            </li>
          </ol>
          <div className="col-md-6 hidden-lg-down text-md-right">
            <div className="share-btns">
              <Link className="mr-3" to={"https://facebook.com/sharer/sharer.php?u=" + window.location.href} target="_blank" aria-label="Share on Facebook" title="Share on Facebook">
                <i className="fab fa-facebook-f text-fb fa-lg" />
              </Link>
              <Link className="mr-3" to={"https://twitter.com/intent/tweet/?text=" + item + "&url=" + window.location.href} target="_blank" aria-label="Share on Twitter" title="Share on Twitter">
                <i className="fab fa-twitter text-twitter fa-lg" />
              </Link>
              <Link className="mr-3" to={"mailto:?subject=" + item + "body=" + window.location.href} target="_self" aria-label="Share by E-Mail" title="Share by E-Mail">
                <i className="far fa-envelope-open text-black fa-lg" />
              </Link>
              <Link className="mr-3" to={"https://pinterest.com/pin/create/button/?url=" + window.location.href + "&media=" + window.location.href + "&description=" + item} target="_blank" aria-label="Share on Pinterest" title="Share on Pinterest">
                <i className="fab fa-pinterest text-pinterest fa-lg" />
              </Link>
            </div>
          </div>
        </div>
        <div>
          <h3 className="text-capitalize text-center border-4-double-white py-1 mt-4 bg-product">{item}</h3>
          <div className="row">
              <div className="col-md-5">
                  <div className="row p-4">
                    <img id="main-img" className="p-4 border-1 img-fluid" src={MasalaChaiJPG} />
                  </div>
                  <div className="row px-4">
                    <div className="col-3">
                      <img className="border-1 img-fluid p-md-2" src={MasalaChaiJPG} onClick={this.handleClick}/>
                    </div>
                    <div className="col-3">
                      <img className="border-1 img-fluid p-md-2" src={GingerChaiJPEG} onClick={this.handleClick}/>
                    </div>
                    <div className="col-3">
                      <img className="border-1 img-fluid p-md-2" src={CinnamonChaiJPG} />
                    </div>
                    <div className="col-3">
                      <img className="border-1 img-fluid p-md-2" src={KashmiriChaiPNG} />
                    </div>
                  </div>
              </div>
              <div className="text-left col-md-7">
                  <h5 className="text-capitalize text-center py-1 mt-4 bg-f7f7f7 d-block d-md-none">Description</h5>
                  <div className="p-lg-5 m-lg-5 p-sm-3 m-sm-3 mt-3 line-height-2 letter-spacing-4">
                    <p>A tea spiced with Indian flavors, and desired greatly on cold mornings. The most common spices used are cardamom, clove, black pepper, cinnamon, ginger and nutmeg. Recipes use some or all of this in varying proportions.</p>
                    <p><span className="font-weight-600">$ 2.75</span><i> per cup</i></p>
                  </div>
              </div>
          </div>
        </div>
        <div className="py-4">
          <h5 className="text-capitalize text-center py-1 mt-4 bg-f7f7f7">Serving</h5>
          <div className="row py-4 px-lg-5 mx-lg-5 text-center">
            <div className="col">
              <i className="fas fa-fire fa-4x text-danger" />
              <div className="pt-2">hot</div>
            </div>
            <div className="col">
              <i className="far fa-clock fa-4x text-primary" />
              <div className="pt-2">4 mins</div>
            </div>
            <div className="col">
              <i className="fas fa-leaf fa-4x text-success" />
              <div className="pt-2">organic</div>
            </div>
            <div className="col">
              <i className="fas fa-shopping-bag fa-4x text-info" />
              <div className="pt-2">packaged</div>
            </div>
            <div className="col">
              <span className="fa-layers fa-4x text-warning">
                <i className="fas fa-certificate" />
                <span className="fa-layers-text fa-inverse" data-fa-transform="shrink-11.5 rotate--30">NEW</span>
              </span>
              <div className="pt-2">new</div>
            </div>
          </div>
        </div>
        <div className="py-4">
          <h5 className="text-capitalize text-center py-1 mt-4 bg-f7f7f7">Ingredients</h5>
          <div className="row py-4">
            <div className="col">
              <img className="circle img-fluid" src={CinnamonJPG} />
              <div className="pt-2 text-center">Cinnamon</div>
            </div>
            <div className="col">
              <img className="circle img-fluid" src={GingerJPG} />
              <div className="pt-2 text-center">Ginger</div>
            </div>
            <div className="col">
              <img className="circle img-fluid" src={CardamomJPG} />
              <div className="pt-2 text-center">Cardamom</div>
            </div>
            <div className="col">
              <img className="circle img-fluid" src={NutmegJPG} />
              <div className="pt-2 text-center">Nutmeg</div>
            </div>
            <div className="col">
              <img className="circle img-fluid" src={BlackPepperJPG} />
              <div className="pt-2 text-center">Black Pepper</div>
            </div>
            <div className="col">
              <img className="circle img-fluid" src={CloveJPG} />
              <div className="pt-2 text-center">Clove</div>
            </div>
            <div className="col">
              <img className="circle img-fluid" src={MilkJPG} />
              <div className="pt-2 text-center">Milk</div>
            </div>
            <div className="col">
              <img className="circle img-fluid" src={TeaJPG} />
              <div className="pt-2 text-center">Tea powder</div>
            </div>
            <div className="col">
              <img className="circle img-fluid" src={SugarJPG} />
              <div className="pt-2 text-center">Sugar</div>
            </div>
          </div>
        </div>
        <div className="py-4">
          <h5 className="text-capitalize text-center py-1 mt-4 bg-f7f7f7">How it is made</h5>
          <div className="line-height-2 py-4 px-lg-5 mx-lg-5">
            <ul>
              <li>In a pan, boil the water and milk together. Add the freshly grated ginger.</li>
              <li>When it starts boiling (and not before that), add the tea powder.</li>
              <li>Boil on low heat for around 2 minutes. Add tulsi and stir.</li>
              <li>Continue boiling on low heat till the colour starts changing and the consistency becomes a little less watery (another 2-3 minutes).</li>
              <li>Strain and serve hot.</li>
            </ul>
          </div>
        </div>
        <FeaturedProducts />
      </div>
    );
  }
}