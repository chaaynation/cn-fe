/*eslint import/no-unresolved: off*/

import React, { Component } from 'react';

import Layout from 'Components/layouts/Layout';
import Loc from 'Components/sections/home/Locations';

export default class Locations extends Component {
  render() {
    return (
        <Layout>
          <main>
            <Loc />
          </main>
        </Layout>
    );
  }
}