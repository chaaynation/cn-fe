/*eslint import/no-unresolved: off*/

import React, { Component } from 'react';

import Layout from 'Components/layouts/Layout';
import AboutCompany from 'Components/sections/AboutCompany';

export default class About extends Component {
  render() {
    return (
        <Layout>
          <main>
            <AboutCompany />
          </main>
        </Layout>
    );
  }
}