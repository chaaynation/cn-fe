/*eslint import/no-unresolved: off*/

import React, { Component } from 'react';

import Layout from 'Components/layouts/Layout';
import ProductDetail from 'Components/sections/ProductDetail';
import PropTypes from 'prop-types';


export default class Product extends Component {
  static get propTypes() {
    return {
      location: PropTypes.object
    };
  }
  render() {
    return (
        <Layout>
          <main className="py-4">
            <ProductDetail loc={this.props.location.pathname} url={this.props.location} />
          </main>
        </Layout>
    );
  }
}