/*eslint import/no-unresolved: off*/

import React, { Component } from 'react';

import Layout from 'Components/layouts/Layout';
import HiW from 'Components/sections/home/HowItWorks';

export default class HowItWorks extends Component {
  render() {
    return (
        <Layout>
          <main>
            <HiW />
          </main>
        </Layout>
    );
  }
}