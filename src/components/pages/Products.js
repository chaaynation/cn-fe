/*eslint import/no-unresolved: off*/

import React, { Component } from 'react';

import Layout from 'Components/layouts/Layout';
import MenuList from 'Components/sections/MenuList';

export default class Products extends Component {
  render() {
    return (
        <Layout>
          <main>
            <MenuList />
          </main>
        </Layout>
    );
  }
}