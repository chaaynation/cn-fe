/*eslint import/no-unresolved: off*/

import React, { Component } from 'react';

import Layout from 'Components/layouts/Layout';
import ContactSection from 'Components/sections/Contact';

export default class Contact extends Component {
  render() {
    return (
        <Layout>
          <main>
            <ContactSection />
          </main>
        </Layout>
    );
  }
}