import 'babel-polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, browserHistory, Route } from 'react-router-dom';
// import routes from './routes';
import configureStore from './store/configureStore';
// import Home from './components/Home';
import About from './components/pages/About';
import Contact from './components/pages/Contact';
import Products from './components/pages/Products';
import Product from './components/pages/Product';
import Locations from './components/pages/Locations';
import Offers from './components/pages/Offers';
import HowItWorks from './components/pages/HowItWorks';
import App from './components/App';
const store = configureStore();

ReactDOM.render(
  (<Provider store={store}>
    <Router  history={browserHistory}>
      <div>
        <Route exact path="/" component={App}/>
        <Route exact path="/products" component={Products}/>
        <Route exact path="/locations" component={Locations}/>
        <Route exact path="/offers" component={Offers}/>
        <Route exact path="/how-it-works" component={HowItWorks}/>
        <Route path="/about" component={About}/>
        <Route path="/contact" component={Contact}/>
        <Route path="/product/:filter" component={Product}/>
      </div>
    </Router>
  </Provider>),
  document.getElementById('app')
);